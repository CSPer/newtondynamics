## What is this repository for?

To test Molecular dynamics, and Monte Carlo based algorithms on simple preferably analytically solvable systems. (Not recommended to be used on more than 3D)

Current implementation is on a 2D system.

**MD integrators:**

* Velocity Verlet
* Leap Frog

**Monte Carlo Schemes**

* Hybrid Monte Carlo
* Hybrid Monte Carlo with a bimodal velocity distribution
* Grand Canonical Monte Carlo (in progress)

## Usage

To run a single simulation:

```bash
python main.py -f parameter_file.mdp
```

To run multiple independent simulations:

```bash
python main_mp.py -f parameter_file.mdp -np number_of_processes
```

## PARAMETERS

**key:** simulation  
**Values:** {'hmc', 'dfhmc', 'mehmc_mdavg', 'mehmc_movavg'}  
**description:** Defines the simulation method to be used  


#### INTEGRATOR PARAMETERS

**key:** integrator 
**values:** {lf, vv}  
**description:** Defines the integrator algorithm to be used for the simulation

**key:** dt  
**values:** float value  
**description:** Timestep to be used for the integrator algorithm

**key:** MDSteps  
**values:** integer value  
**description:** The number of integrator step to be used

**key:** HMCSteps  
**values:** integer value  
**description:** The number of HMC steps to be used

#### SYSTEM PARAMETERS

**key:** kT  
**values:** floating point number  
**description:** Boltzmann constant times the temperature. Has units of energy and is related to the variance of the velocity distribution.

**key:** x0  
**values:** list of floating point numbers, [value, value] 
**description:** the starting positions for the system

#### IO FILE PARAMETERS

**key:** trjout 
**values:** integer number  
**description:** the stride of writing out to trajectory file

**key:** trjfile  
**values:** string  
**description:** The trajectory filename

**key:** logfile  
**values:** string  
**description:** the log filename 

#### FILTER PARAMETERS

**key:** scl  
**values:** floating point number  
**description:** The scaling factor to be multiplied with the filtered velocities. This has the effect of scaling the Filter response function by scl. This option is required for 'DFHMC' and 'MEHMC' type simulations

**key:** biasfreq  
**values:** integer value  
**description:** The stride to apply the filter

**key:** filter_file  
**values:** string  
**description:** path to the filter coefficient file.

**key:** biasbuffer  
**values:**    
**description:**


