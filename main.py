"""
    Copyright(c) Can Pervane
    Author  : Can Pervane
    Date    : 25 Feb 2016
    Name    : main.py
    Purpose : it acts as an interface to the code in the src
"""

# import os
# import sys
# MAIN_DIR = os.path.dirname(os.path.abspath(__file__))
# sys.path.append(os.path.join(MAIN_DIR, 'src'))

import random
import argparse
from MCMC import MCMC
from fileIO.read import parse_mdp
from sys import exit


parser = argparse.ArgumentParser()
req_grp = parser.add_argument_group(title='Required Optional')
req_grp.add_argument("-f", "--file_name", required=True, help="enter name of .mdp file")
args = parser.parse_args()


param=parse_mdp(args.file_name)
simulation = param['simulation']
if simulation == 'mehmc':
    biasavg = param['biasavg']
    if biasavg == 'moving':
        simulation = 'mehmc_movavg'
    elif biasavg == 'fullmd':
        simulation = 'mehmc_mdavg'
    else:
        print 'biasavg, should be either moving or fullmd'
        exit()

seed = 123484
x=MCMC[simulation](param, seed)
print x



