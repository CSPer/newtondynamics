import numpy as np
#force[myvar](parameter1, parameter2)

def Ek(v):
    """
        Calculate the total kinetic energy
    """
    return sum(0.5*(v**2))

def twoWell_P(x,a=500,b=2):
    """
        twoWell Potential Energy. the default parameters are such
        that the motion along x0 is much faster than along x1
        'a*x0**2 + (x1**2)*((x1-b)**2)'
    """
    return a*x[0]**2 + (x[1]**2)*((x[1]-b)**2)

def twoWell_F(x,a=500,b=2):
    """
        The Force components of the twoWell Potential
        -dV/dx
        f1 = '-2*a*x0'
        f2 = '-2*x1*(x1-2)**2-2*(x1**2)*(x1-2)'
    """
    f1 = -2*a*x[0]
    f2 = -2*x[1]*(x[1]-2)**2-2*(x[1]**2)*(x[1]-2)
    return np.array([f1,f2])

def harmonic_P(x):
    return sum(500*x**2)

def harmonic_F(x):
    return -1000*x

# Dictionary that has the function as values. 
# When defining new potential and Force functions add it 
# to this dictionary
Force = {
    'twoWell': twoWell_F,
    'harmonic' : harmonic_F
}

Ep = {
    'twoWell': twoWell_P,
    'harmonic' : harmonic_P
}

if __name__ == '__main__':
    x=[1.0,2.0]
    f = Force['twoWell'](x)
    print "Fx: " + str(f[0])
    print "Fy: " + str(f[1])