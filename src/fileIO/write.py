"""
    Copyright(c) Can Pervane
    Author  : Can Pervane
    Date    : 29 Feb 2016
    Name    : write.py
    Purpose : Provide functions for writing to files
              during or at the end of a simulation
"""
import math

def writeTrj(x, v, t, filename='trj.txt', sep=','):
    f=open(filename,'a')
    f.write(str(t))
    for dim in range(len(x)):
        f.write(sep + str(x[dim]))
    for dim in range(len(v)):
        f.write(sep + str(v[dim]))
    f.write('\n')
    f.close()

def writeLog_var(message, var, filename='log.txt'):
    f=open(filename, 'a')
    f.write(message +' = ' + str(var) + '\n')
    f.close

def writeLog_text(message, filename='log.txt'):
    f=open(filename, 'a')
    f.write(message + '\n')
    f.close

def writeLog_describe(message, filename='log.txt'):
    max_words = 10
    f=open(filename, 'a')
    words = message.split(' ')
    lines = math.ceil(len(words)/float(max_words))
    f.write('\n')
    for i in range(int(lines)):
        if i == lines-1:
            f.write(' '.join(words[i*max_words:]) + '\n')
        else:
            f.write(' '.join(words[i*max_words:(i+1)*max_words]) + '\n')
    f.write('\n')
    f.close

def writeLog_mess(message, box='*', filename='log.txt'):
    f=open(filename, 'a')
    buffer=8
    for i in range(len(message) + buffer):
        f.write(box)
    f.write('\n')
    f.write(box + '   ' + message + '   ' + box + '\n')
    for i in range(len(message) + buffer):
        f.write(box)
    f.write('\n')
    f.close


