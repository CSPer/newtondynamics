"""
    Copyright(c) Can Pervane
    Author  : Can Pervane
    Date    : 29 Feb 2016
    Name    : read.py
    Purpose : Provide functions for reading files
              such as;
                    simulation input parameters
                    Filter Coefficients
                    etc.
"""

import pandas as pd
import numpy as np

def parse_trj(filename):
    df=pd.read_csv(filename,header=None, na_values=['--'])
    df.columns=['Step', 'px', 'py', 'vx', 'vy']
    return df

def parse_mdp(filename):
    mdp_dic = {}
    with open(filename) as f:
        lines = f.readlines()
        for line in lines:
            if line.strip():
                if not(line.lstrip()[0] == ';'):
                    key, value=mdp_line(line)
                    mdp_dic[key] = value
    return mdp_dic

def mdp_line(line):
    key_value = line.split(';',1)[0]
    tmp = key_value.split('=')
    key = tmp[0].strip()
    value = tmp[1].strip()
    return key,value

def parse_filter(filename):
    coeff = []
    with open(filename) as f:
        lines = f.readlines()
        for line in lines:
            coeff.append(float(line.strip()))
    return np.array(coeff)