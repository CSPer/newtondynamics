"""
    Copyright(c) Can Pervane
    Author  : Can Pervane
    Date    : 27 Feb 2016
    Name    : LF.py
    Purpose : leap frog numerical integrator
"""
from potential import Force
import numpy as np

def LFupdateV_Half(v, f, dt):
    """
        Half Step Velocity Update
    """
    return v + 0.5*f*dt

def LFupdateV_Full(v, f, dt):
    """
        Full step Velocity update 
        v(t+0.5dt)= v(t-0.5dt) + f(t)dt
    """
    return v + f*dt

def LFupdateP(x, v, dt):
    """
        Full Step Position Update
        x(t+dt) = x(t) + v(t+0.5dt)dt
    """
    return x + v*dt

def MD_LF_vel(Steps, x, v, dt, potential='twoWell'):
    """
        The last return value correspond to the positon and Velocity
        vector at time step (steps)dt
        ex:
            if 
                steps: 100
                dt   : 0.01 s
            then
                return v(1s) x(1s)
    """
    x = np.array(x)
    v = np.array(v)
    trjv = []
    trjv.append(v)
    #Calculate v(t+0.5dt), from f(t) v(t)
    f = Force[potential](x)
    v = LFupdateV_Half(v, f, dt)
    for i in range(Steps):
        x = LFupdateP(x, v, dt)
        f = Force[potential](x)
        v = LFupdateV_Full(v, f, dt)
        # x(t+dt), v(t+0.5dt)
    v = LFupdateV_Half(v, f, -dt)
    # x(t+(steps)*dt), v(t+(steps)*dt)
    trjv.append(v)
    return np.array(trjv).flatten()

def MD_LF(Steps, x, v, dt, avg=False, potential='twoWell'):
    """
        The last return value correspond to the positon and Velocity
        vector at time step (steps)dt
        ex:
            if 
                steps: 100
                dt   : 0.01 s
            then
                return v(1s) x(1s)
    """
    x = np.array(x)
    v = np.array(v)
    b = np.array([0.0,0.0])
    b = b + v
    #Calculate v(t+0.5dt), from f(t) v(t)
    f = Force[potential](x)
    v = LFupdateV_Half(v, f, dt)
    b = b + 0.5*v
    for i in range(Steps):
        x = LFupdateP(x, v, dt)
        f = Force[potential](x)
        v = LFupdateV_Full(v, f, dt)
        b = b + v
        # x(t+dt), v(t+0.5dt)
    b = b - 0.5*v
    v = LFupdateV_Half(v, f, -dt)
    # x(t+(step-1)*dt), v(t+(step-1)*dt)
    if avg:
        return b/float(Steps+1)
    else:
        return x, v

def MD_LF_avg(Steps, x, v, dt, L, b=[0.0, 0.0], potential='twoWell'):
    """
        The last return value correspond to the positon and Velocity
        vector at time step (steps)dt
        ex:
            if 
                steps: 100
                dt   : 0.01 s
            then
                return v(1s) x(1s)
    """
    x = np.array(x)
    v = np.array(v)
    b = np.array(b)
    #Calculate v(t+0.5dt), from f(t) v(t)
    f = Force[potential](x)
    b = (1.0-1.0/L)*b + (1.0/L)*v
    v = LFupdateV_Half(v, f, dt)
    #b(t+0.5dt) = b(t) + v(t+0.5dt)
    # b = (1.0-0.5/L)*b + 0.5/L*v
    for i in range(Steps):
        x = LFupdateP(x, v, dt)
        f = Force[potential](x)
        v_ = LFupdateV_Full(v, f, dt)
        # b() = b(t+0.5dt+idt) + v(t+0.5dt+(i+1)dt)
        b = (1.0-1.0/L)*b + (0.5/L)*(v+v_)
        v = v_
        # x(t+dt), v(t+0.5dt)
    v = LFupdateV_Half(v, f, -dt)
    #b = (1.0-0.5/L)*b + 0.5/L*v
    # x(t+(step-1)*dt), v(t+(step-1)*dt)
    return x, v, b