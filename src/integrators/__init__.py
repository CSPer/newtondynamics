from integrators.VV import MD_VV, MD_VV_avg, MD_VV_vel
from integrators.LF import MD_LF, MD_LF_avg, MD_LF_vel

MD = {
    'vv': MD_VV,
    'lf': MD_LF
}

MD_avg = {
    'vv': MD_VV_avg,
    'lf': MD_LF_avg
}

MD_v = {
    'vv': MD_VV_vel,
    'lf': MD_LF_vel
}
