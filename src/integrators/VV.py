"""
    Copyright(c) Can Pervane
    Author  : Can Pervane
    Date    : 25 Feb 2016
    Name    : VV.py
    Purpose : Velocity Verlet numerical integrator
"""
from potential import Force
import numpy as np

def VVupdateV(v, f, dt):
    """
        Half step Velocity update
    """
    return v + 0.5*f*dt
        
def VVupdateP(x,v, dt):
    """
        Full step Position update
    """
    return x + v*dt

def MD_VV_vel(Steps, x, v, dt, avg=False, potential='twoWell'):
    """
        An MD algorithm that uses Velocity Verlet as its integration
        The last return value correspond to the positon and Velocity
        vector at time step steps*dt
        ex:
            if 
                steps: 100
                dt   : 0.01 s
            then
                return v(1s) x(1s)
    """
    x=np.array(x)
    v=np.array(v)
    trjv = []
    for i in range(Steps+1):
        f = Force[potential](x)
        if i != 0:
            v = VVupdateV(v, f, dt)
        trjv.append(v)
        if i != Steps:
            v = VVupdateV(v, f, dt)
            x = VVupdateP(x, v, dt)
    return np.array(trjv).flatten()

def MD_VV(Steps, x, v, dt, avg=False, potential='twoWell'):
    """
        An MD algorithm that uses Velocity Verlet as its integration
        The last return value correspond to the positon and Velocity
        vector at time step steps*dt
        ex:
            if 
                steps: 100
                dt   : 0.01 s
            then
                return v(1s) x(1s)
    """
    x=np.array(x)
    v=np.array(v)
    b = np.array([0.0, 0.0])
    for i in range(Steps+1):
        f = Force[potential](x)
        if i != 0:
            v = VVupdateV(v, f, dt)
        if avg:
            b = b + v
        if i != Steps:
            v = VVupdateV(v, f, dt)
            x = VVupdateP(x, v, dt)
    if avg:
        return b/float(Steps+1)
    else:
        return x, v

def MD_VV_avg(Steps, x, v, dt, L, b=[0.0, 0.0], potential='twoWell'):
    """
        An MD algorithm that uses Velocity Verlet as its integration
        The last return value correspond to the positon and Velocity
        vector at time step steps*dt
        ex:
            if 
                steps: 100
                dt   : 0.01 s
            then
                return v(1s) x(1s)
    """
    x=np.array(x)
    v=np.array(v)
    b = np.array(b)
    for i in range(Steps+1):
        f = Force[potential](x)
        if i != 0:
            v = VVupdateV(v, f, dt)
        b = (1-1.0/L)*b + (1.0/L)*v
        if i != Steps:
            v = VVupdateV(v, f, dt)
            x = VVupdateP(x, v, dt)
    return x, v, b
