"""
    Copyright(c) Can Pervane
    Author  : Can Pervane
    Date    : 29 Feb 2016
    Name    : figure.py
    Purpose : Contains functions to configure a figure
"""


def AddAxes(f,x,y, nrow, ncol, x_margin=0.1, y_margin=0.1, x_space=0.05, y_space=0.05):
    """
        Lays out plots on a grid, each plot is
        represented by a point on the grid 
        nrow: number of rows 
        ncol: number of columns
        x,y : represents the active plot
    """

    dx = (1-2*x_margin-(ncol-1)*x_space)/float(ncol)
    dy = (1-2*y_margin-(nrow-1)*y_space)/float(nrow)
    if ncol==1:
        ax=f.add_axes([0.1, 0.1+ (dy + y_space)*y, 0.7, dy])
    elif nrow==1:
        ax=f.add_axes([0.1+ (dx + x_space)*x, 0.15, dx, 0.75])
    else:
        ax=f.add_axes([0.1+ (dx + x_space)*x, 0.1+ (dy + y_space)*y, dx, dy])
    return ax