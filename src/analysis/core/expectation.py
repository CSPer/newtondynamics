import numpy as np
from potential import Ep

def marginal_prob(dim_i, kT, pot='twoWell'):
    """
        P(r0|r1,r2,..) = \int exp(-beta*Ep(r)) dr1dr2..
        P(r0|r1,r2,..) = \sum exp(-beta*Ep(r)) dr1dr2..
    """

    beta = 1.0/kT
    d=[None]*2
    x=[None]*2
    d[0] = 0.001
    d[1] = 0.01
    x[0] = np.arange(-0.15, 0.15, d[0])
    x[1] = np.arange(-1.0, 3.0, d[1])
    P = []
    if dim_i == 0:
        for i in range(len(x[0])):
            a = np.ones(len(x[1]))*x[0][i]
            P.append( sum( np.exp( -beta*Ep[pot]([a,x[1]]) ) )*d[1] )
    if dim_i == 1:
        for i in range(len(x[1])):
            a = np.ones(len(x[0]))*x[1][i]
            P.append( sum( np.exp( -beta*Ep[pot]([x[0], a]) ) )*d[0] )
    P = np.array(P)
    norm = np.trapz(P, dx = d[dim_i])
    P = P / norm
    return x[dim_i], P

def pmf_t(dim_i, kT, pot='twoWell'):
    x_, P = marginal_prob(dim_i, kT, pot=pot)
    tmp = -kT*np.log(P)
    pmf = tmp - min(tmp)
    return x_ , pmf