import numpy as np


def pmf_s(data, nbin, kT):
    density, bins = np.histogram(data, nbin, density=True)
    mids = []
    pmf = []
    print len(density)
    print len(bins)
    print density[0]
    for j in range(len(bins)-1):
        if density[j] != 0.0:
            mids.append(bins[j] + 0.5*(bins[j+1] - bins[j]))
            pmf.append(-kT*np.log(density[j]))
    pmf = np.array(pmf)
    return np.array(mids), pmf-min(pmf)
