from fileIO.read import parse_filter
from signals.sinusoidal import sin, FIR, MovingAvg, FIR2, FIR_Filter
import matplotlib.pyplot as plt
import argparse
import random
import numpy as np
from matplotlib import rc
from analysis.core.figure import AddAxes
rc('text', usetex=True)

parser = argparse.ArgumentParser()
req_grp = parser.add_argument_group(title='Required Optional')
req_grp.add_argument("-f", "--file_name", required=True, help="enter name of fiter file")
args = parser.parse_args()

coeff = parse_filter(args.file_name)

#Calculate superposition sin
nSample = 1500
dt = 0.01
t1, y1 = sin(nSample, dt, 1, 0)
t2, y2=sin(nSample, dt, 10, 0)
signal = y2 + y1
out = MovingAvg(signal, 14)
out2 = FIR_Filter(signal, coeff)

# # Plot the super position of sinwaves and the filtered out put signal
# fig_label = ['a)','b)']
# f =  plt.figure(figsize=(10,6), dpi=100)
# ax1=AddAxes(f,0,1, 2,1, y_space=0.15, x_space=0.1, y_margin=0.1, x_margin=0.1)
# plt.plot(t2, signal, label='Input \nSignal')
# ax1.set_xlim([5, 10])
# ax1.tick_params(axis='y', labelsize=15)
# ax1.tick_params(axis='x', labelsize=15)
# ax1.axhline(y=0, ls='-.', color='k')
# plt.xlabel('$Time (s)$', size=20)
# plt.ylabel('$Amplitude$', size=20)
# ax1.set_title('$'+fig_label[0]+'$', size=20)
# plt.legend(bbox_to_anchor=(1.0, 1.0),prop={'size':15}, loc=2, borderaxespad=0., frameon=False)

# ax1=AddAxes(f,0,0, 2,1, y_space=0.15, x_space=0.1, y_margin=0.1, x_margin=0.1)
# plt.plot(t2[500:-500],out2, label='Output \nSignal')
# ax1.axhline(y=0, ls='-.', color='k')
# ax1.tick_params(axis='y', labelsize=15)
# ax1.tick_params(axis='x', labelsize=15)
# plt.xlabel('$Time (s)$', size=20)
# plt.ylabel('$Amplitude$', size=20)
# ax1.set_title('$'+fig_label[1]+'$', size=20)
# plt.legend(bbox_to_anchor=(1.0, 1.0),prop={'size':15}, loc=2, borderaxespad=0., frameon=False)

# # plt.plot(t2[7:-7],out, 'r')
# f.savefig('/home/csp1g13/Dropbox/PhD/Report/fig/DF-LowPass.pdf')


#Calculate filter response
nSample = len(coeff)
dt = 0.001
f = 1.1
df = 0.2
amp1 = []
amp2 = []
freq = []
f_nyg = 1.0/(2*dt)
for i in xrange(2495):
    #f = f + df
    t, y = sin(nSample, dt, f, 0)
    #amp1.append(FIR(y,14))
    amp2.append(FIR2(y,coeff))
    freq.append(f/f_nyg)
    f = f + df

f =  plt.figure(figsize=(10,6), dpi=100)
ax1=f.add_axes([0.1, 0.1, 0.8, 0.8])
ax1.tick_params(axis='y', labelsize=15)
ax1.tick_params(axis='x', labelsize=15)
ax1.set_ylim([-0.1, 1.1])
plt.xlabel('$f_{normalised} (f/f_{Nyquist})$', size=20)
plt.ylabel('$A_{filtered}/A_{signal}$', size=20)
#plt.plot(freq, amp1, 'r')
plt.plot(freq, amp2, 'b')
plt.suptitle('$Frequency \quad Response$', size=20)
f.savefig('/Users/canpervane/Dropbox/PhD/Report/fig/DF-fResponse.pdf')

plt.show()

