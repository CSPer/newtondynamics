"""
    Copyright(c) Can Pervane
    Author  : Can Pervane
    Date    : 29 Feb 2016
    Name    : test_ensemble.py
    Purpose : Interface to test the HMC algorithm
"""

# import os
# import sys
# MAIN_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
# sys.path.append(os.path.join(MAIN_DIR, 'src'))

import math
import argparse
import matplotlib.pyplot as plt
import numpy as np
from fileIO.read import parse_trj, parse_mdp
from analysis.core.figure import AddAxes

parser = argparse.ArgumentParser()
req_grp = parser.add_argument_group(title='Required Optional')
req_grp.add_argument("-f1", "--file_name1", required=True, help="enter name of .trj file")
req_grp.add_argument("-f2", "--file_name2", required=True, help="enter name of .mdp file")
args = parser.parse_args()

# these parameters should only be written in mdp file
df = parse_trj(args.file_name1)
param=parse_mdp(args.file_name2)

x2_t = float(param['x2'])
y2_t = float(param['y2'])
trjout = int(param['trjout'])
check = int(param['check'])
check = check/trjout
calcs = int(math.floor(len(df['px']) / check))

PE_x = []
PE_y = []
step = []
print len(df['Step'])
print calcs
for i in range(calcs): 
    x2 = np.mean((df['px'][:(i+1)*check])**2)
    y2 = np.mean((df['py'][:(i+1)*check])**2)
    step.append(df['Step'][(i+1)*check-1])
    PE_x.append(abs(100*((x2-x2_t)/x2_t)))
    PE_y.append(abs(100*((y2-y2_t)/y2_t)))

f =  plt.figure(figsize=(10,6), dpi=100)
ax1=AddAxes(f,0,0, 2,1, y_space=0.1, x_space=0.1, y_margin=0.1, x_margin=0.1)
ax1.set_yscale('log')
plt.plot(np.log10(step), PE_x, 'b-')
plt.xlabel('HMCStep')
plt.ylabel('PE(x^2)')

ax1=AddAxes(f,0,1, 2,1, y_space=0.1, x_space=0.1, y_margin=0.1, x_margin=0.1)
ax1.set_yscale('log')
plt.plot(np.log10(step), PE_y, 'b-')
plt.xlabel('HMCSteps')
plt.ylabel('PE(y^2)')

plt.suptitle('Percent Error of <a^2> - HMCStep : a stands for either x or y')

plt.show()