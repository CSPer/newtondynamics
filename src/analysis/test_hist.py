"""
    Copyright(c) Can Pervane
    Author  : Can Pervane
    Date    : 29 Feb 2016
    Name    : test_hist.py
    Purpose : Interface to test the HMC algorithm
"""

# import os
# import sys
# MAIN_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
# sys.path.append(os.path.join(MAIN_DIR, 'src'))

import argparse
import matplotlib.pyplot as plt
import numpy as np
from fileIO.read import parse_trj
from analysis.core.figure import AddAxes
from analysis.core.expectation import marginal_prob

parser = argparse.ArgumentParser()
req_grp = parser.add_argument_group(title='Required Optional')
req_grp.add_argument("-f", "--file_name", required=True, help="enter name of .trj file")
args = parser.parse_args()

# these parameters should only be written in mdp file
df = parse_trj(args.file_name)
kT=1.0

px, Prob_x = marginal_prob(0,kT)
py, Prob_y = marginal_prob(1,kT)
f =  plt.figure(figsize=(10,6), dpi=100)
ax1=AddAxes(f,0,0, 2,2, y_space=0.1, x_space=0.1, y_margin=0.1, x_margin=0.1)
plt.hist(df['px'],100, normed=True)
plt.plot(px, Prob_x, 'r-')
plt.xlabel('px')
ax2=AddAxes(f,1,0, 2,2, y_space=0.1, x_space=0.1, y_margin=0.1, x_margin=0.1)
plt.hist(df['py'],100, normed=True)
plt.plot(py, Prob_y, 'r-')
plt.xlabel('py')
ax3=AddAxes(f,0,1, 2,2, y_space=0.1, x_space=0.1, y_margin=0.1, x_margin=0.1)
plt.hist(df['vx'],100, normed=True)
plt.xlabel('vx')
ax4=AddAxes(f,1,1, 2,2, y_space=0.1, x_space=0.1, y_margin=0.1, x_margin=0.1)
plt.hist(df['vy'],100, normed=True)
plt.xlabel('vy')

plt.suptitle('Boltzmann Prob - Positional and Velocity coordinate ')
plt.show()


