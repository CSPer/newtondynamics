"""
    Copyright(c) Can Pervane
    Author  : Can Pervane
    Date    : 29 Feb 2016
    Name    : test_trj.py
    Purpose : Interface to test the HMC algorithm
"""

# import os
# import sys
# MAIN_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
# sys.path.append(os.path.join(MAIN_DIR, 'src'))

import argparse
import matplotlib.pyplot as plt
import numpy as np
from fileIO.read import parse_bias
from analysis.core.figure import AddAxes

parser = argparse.ArgumentParser()
req_grp = parser.add_argument_group(title='Required Optional')
req_grp.add_argument("-f", "--file_name", required=True, help="enter name of .trj file")
args = parser.parse_args()

# these parameters should only be written in mdp file
df = parse_bias(args.file_name)

f =  plt.figure(figsize=(10,6), dpi=100)
ax1=AddAxes(f,1,0, 1,1, y_space=0.1, x_space=0.1, y_margin=0.1)
ax1.tick_params(axis='y', labelsize=15)
ax1.tick_params(axis='x', labelsize=15)
plt.plot(df['Step'], df['bias0'], 'b-', label='$<v_x>$')
plt.plot(df['Step'], df['bias1'], 'r-', label='$<v_y>$')
plt.xlabel('$HMCSteps$', size=20)
plt.ylabel('$<v_x>, <v_y>$', size=20)
plt.suptitle('$(<v_x>,<v_y>) - HMCSteps: kT=0.1$',size=20)
plt.legend(bbox_to_anchor=(0.8, 1.0),prop={'size':15}, loc=2, borderaxespad=0., frameon=False)
#plt.show()
f.savefig('/home/csp1g13/Dropbox/PhD/Report/fig/Avg_vel1.pdf')