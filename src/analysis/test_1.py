"""
    Copyright(c) Can Pervane
    Author  : Can Pervane
    Date    : 29 Feb 2016
    Name    : test_hist.py
    Purpose : Interface to test the HMC algorithm
"""

# import os
# import sys
# MAIN_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
# sys.path.append(os.path.join(MAIN_DIR, 'src'))

import argparse
import matplotlib.pyplot as plt
import numpy as np
from fileIO.read import parse_trj
from analysis.core.figure import AddAxes

parser = argparse.ArgumentParser()
req_grp = parser.add_argument_group(title='Required Optional')
req_grp.add_argument("-f", "--file_name", required=True, help="enter name of .trj file")
args = parser.parse_args()

# these parameters should only be written in mdp file
df = parse_trj(args.file_name)

f =  plt.figure(figsize=(10,6), dpi=100)
ax1=AddAxes(f,0,0, 2,1, y_space=0.1, x_space=0.1, y_margin=0.1, x_margin=0.1)
plt.plot(df['p0'], df['Step'], 'b-')
plt.gca().invert_yaxis()
plt.xlabel('x')
plt.ylabel('GCMCSteps')

ax1=AddAxes(f,0,1, 2,1, y_space=0.1, x_space=0.1, y_margin=0.1, x_margin=0.1)
plt.hist(df['p0'], 100, normed=True)
plt.xlabel('x')
plt.ylabel('Frequency')

plt.suptitle('Position Coordinate (x,y) - HMCSteps')

plt.show()