"""
    Copyright(c) Can Pervane
    Author  : Can Pervane
    Date    : 29 Feb 2016
    Name    : test_pmf.py
    Purpose : Interface to test the HMC algorithm
"""

# import os
# import sys
# MAIN_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
# sys.path.append(os.path.join(MAIN_DIR, 'src'))

import argparse
import matplotlib.pyplot as plt
import numpy as np
from fileIO.read import parse_trj
from analysis.core.figure import AddAxes
from analysis.core.expectation import pmf_t
from analysis.core.simulation import pmf_s

parser = argparse.ArgumentParser()
req_grp = parser.add_argument_group(title='Required Optional')
req_grp.add_argument("-f", "--file_name", required=True, help="enter name of .trj file")
args = parser.parse_args()

df = parse_trj(args.file_name)
kT=0.1
nbin = 100

px_t, pmf_x_t = pmf_t(0,kT)
py_t, pmf_y_t = pmf_t(1,kT)

px_s, pmf_x_s = pmf_s(df['px'], nbin, kT)
py_s, pmf_y_s = pmf_s(df['py'], nbin, kT)

f =  plt.figure(figsize=(10,6), dpi=100)
ax1=AddAxes(f,0,0, 1,2, y_space=0.1, x_space=0.1, y_margin=0.1, x_margin=0.1)
plt.plot(px_s, pmf_x_s, 'b.')
plt.plot(px_t, pmf_x_t, 'r-')
plt.xlabel('px')
plt.ylabel('PMF(px)')

ax1=AddAxes(f,1,0, 1,2, y_space=0.1, x_space=0.1, y_margin=0.1, x_margin=0.1)
plt.plot(py_s, pmf_y_s, 'b.')
plt.plot(py_t, pmf_y_t, 'r-')
plt.xlabel('py')
plt.ylabel('PMF(py)')

plt.suptitle('PMF (Marginal Prob) - Positional Coordinate')

plt.show()
