"""
    Copyright(c) Can Pervane
    Author  : Can Pervane
    Date    : 29 Feb 2016
    Name    : test_ensemble.py
    Purpose : Interface to test the HMC algorithm
"""

# import os
# import sys
# MAIN_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
# sys.path.append(os.path.join(MAIN_DIR, 'src'))

import math
import argparse
import matplotlib.pyplot as plt
from matplotlib import rc
import numpy as np
from fileIO.read import parse_trj, parse_mdp
from analysis.core.figure import AddAxes
import pandas as pd
rc('text', usetex=True)

parser = argparse.ArgumentParser()
req_grp = parser.add_argument_group(title='Required Optional')
req_grp.add_argument("-f1", "--file_name1", nargs='*', required=True, help="enter name of .trj file")
req_grp.add_argument("-f2", "--file_name2", required=True, help="enter name of .mdp file")
args = parser.parse_args()

# these parameters should only be written in mdp file
param=parse_mdp(args.file_name2)
x2_t = float(param['x2'])
y2_t = float(param['y2'])
trjout = int(param['trjout'])
HMCSteps = int(param['HMCSteps'])
increment = float(param['increment'])
start = float(param['start'])

stop = np.log10(HMCSteps)
calc = np.arange(start, stop, increment)
calc = 10**calc
calc = calc - (calc % trjout)

nfiles = 0
for trjfile in args.file_name1:
    df = parse_trj(trjfile)
    step = []
    PE_x = []
    PE_y = []
    counter = 0
    namex = "".join(['PEx_',str(nfiles+1)])
    namey = "".join(['PEy_',str(nfiles+1)])
    for i in calc:
        step.append(int(i))
        index = list(df['Step']).index(step[counter])
        x2 = np.mean((df['px'][:index+1])**2)
        y2 = np.mean((df['py'][:index+1])**2)
        PE_x.append(100*((x2-x2_t)/x2_t))
        PE_y.append(100*((y2-y2_t)/y2_t))
        counter = counter + 1
    if nfiles == 0:
        df_PEx = pd.DataFrame({'Step': step})
        df_PEx[namex] = PE_x
        df_PEy = pd.DataFrame({'Step': step})
        df_PEy[namey] = PE_y
    else:
        df_PEx[namex] = PE_x
        df_PEy[namey] = PE_y
    nfiles = nfiles + 1

# Calculate the root mean square PE
df_PEx['rms'] = np.sqrt(((df_PEx.ix[:,1:])**2).mean(axis=1))
df_PEy['rms'] = np.sqrt(((df_PEy.ix[:,1:])**2).mean(axis=1))
print df_PEx
print df_PEy

f =  plt.figure(figsize=(10,6), dpi=100)
ax1=AddAxes(f,0,0, 2,1, y_space=0.1, x_space=0.1, y_margin=0.1, x_margin=0.1)
ax1.set_yscale('log')
plt.plot(np.log10(df_PEx['Step']), df_PEx['rms'], 'k.')
plt.plot(np.log10(df_PEx['Step']), df_PEx['rms'], 'b-')
plt.xlabel('$log_{10}(HMC_{STEP})$')
plt.ylabel('$PE(x^2)$')

ax1=AddAxes(f,0,1, 2,1, y_space=0.1, x_space=0.1, y_margin=0.1, x_margin=0.1)
ax1.set_yscale('log')
plt.plot(np.log10(df_PEy['Step']), df_PEy['rms'], 'k.')
plt.plot(np.log10(df_PEy['Step']), df_PEy['rms'], 'b-')
plt.xlabel('$log_{10}(HMC_{STEP})$')
plt.ylabel('$PE(y^2)$')

plt.suptitle('Percent Error of $<a^2>$ - HMCStep : $a$ stands for either $x$ or $y$')

plt.show()




