"""
    Copyright(c) Can Pervane
    Author  : Can Pervane
    Date    : 29 Feb 2016
    Name    : test_trj.py
    Purpose : Interface to test the HMC algorithm
"""

# import os
# import sys
# MAIN_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
# sys.path.append(os.path.join(MAIN_DIR, 'src'))

import argparse
import matplotlib.pyplot as plt
from matplotlib import rc
import numpy as np
from fileIO.read import parse_trj
from analysis.core.figure import AddAxes
rc('text', usetex=True)

parser = argparse.ArgumentParser()
req_grp = parser.add_argument_group(title='Required Optional')
req_grp.add_argument("-f1", "--file_name1", required=True, help="enter name of .trj file")
req_grp.add_argument("-f2", "--file_name2", required=True, help="enter name of .trj file")
args = parser.parse_args()

# these parameters should only be written in mdp file
df1 = parse_trj(args.file_name1)
df2 = parse_trj(args.file_name2)

fig_label = ['a)','b)']
f =  plt.figure(figsize=(10,6), dpi=100)
ax1=AddAxes(f,0,1, 2,1, y_space=0.15, x_space=0.1, y_margin=0.12, x_margin=0.1)
ax1.tick_params(axis='y', labelsize=15)
ax1.tick_params(axis='x', labelsize=15)
ax1.set_title('$'+fig_label[0]+'$', size=20)
plt.plot(df1['Step']/(10**6), df1['p1'], 'b-', label='HMC')
plt.xlabel('$10^6 \quad HMCSteps$', size=20)
plt.ylabel('$y$', size=20)
plt.legend(bbox_to_anchor=(1.0, 1.0),prop={'size':15}, loc=2, borderaxespad=0., frameon=False)

ax1=AddAxes(f,0,0, 2,1, y_space=0.15, x_space=0.1, y_margin=0.13, x_margin=0.1)
ax1.tick_params(axis='y', labelsize=15)
ax1.tick_params(axis='x', labelsize=15)
ax1.set_title('$'+fig_label[1]+'$', size=20)
plt.plot(df2['Step']/(10**6), df2['p1'], 'b-', label='DFHMC')
plt.xlabel('$10^6 \quad HMCSteps$', size=20)
plt.ylabel('$y$', size=20)

plt.suptitle('$Position \quad Coordinate (y) - HMCSteps$',size=20)
plt.legend(bbox_to_anchor=(1.0, 1.0),prop={'size':15}, loc=2, borderaxespad=0., frameon=False)
f.savefig('/Users/canpervane/Dropbox/PhD/Report/fig/SD-positions.pdf')
plt.show()