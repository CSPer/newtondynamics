import numpy as np

def rad2deg(phi):
    deg = phi*(180.0/np.pi)
    return deg

def deg2rad(deg):
    phi = deg*(np.pi/180.0)
    return phi

def sin(N, dt, f, deg):
    phi = deg2rad(deg)
    t = np.arange(0,N*dt,dt)
    w = 2*np.pi*f
    return t, np.sin(w*t + phi)

def MovingAvg(signal, np):
    n = len(signal) - np
    out = []
    for i in xrange(n): 
        start = i
        end = i + np
        out.append(sum(signal[start:end])/float(np))
    return out

def FIR_Filter(signal, coeff):
    np = len(coeff)
    n = len(signal) - np + 1
    out = []
    for i in xrange(n): 
        start = i
        end = i + np
        out.append(sum(signal[start:end]*coeff))
    return out

def FIR(signal, np):
    """
        Frequecny response for moving average
    """
    y = sum(signal[0:np])/float(np)
    amp = abs(y / signal[(np)/2])
    return amp

def FIR2(signal, coeff):
    """
        Freq response for FIR filter
    """
    np=len(coeff)
    y = sum(signal[0:np]*coeff)
    amp = abs(y / signal[(np-1)/2])
    return amp
