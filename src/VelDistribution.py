import numpy as np
import random

def bimodal(mu, sigma):
    X = random.random()
    if X < 0.5:
        return random.gauss(mu, sigma)
    else:
        return random.gauss(-mu, sigma)

def VelBimodal_SV(bias, kT):
    """
        Build up a velocity vector by drawing RND from singleVariate 
        Bimodal Normal distribution with:
            mean(mu)=bias
            std(sigma)=sqrt(kT)
        Which is equal to drawing a RND with 1/2 probablity from 
        N(bias[i],sigma) or N(-bias[i],sigma) for each dof 
    """
    sigma = np.sqrt(kT)
    v = []
    for mu in bias:
        v.append(bimodal(mu,sigma))
    return np.array(v)

def VelBimodal_MV(bias, kT):
    """
        Build up a velocity vector by drawing RND from multivariate 
        Bimodal Normal distribution with:
            mean(mu)=bias
            std(sigma)=sqrt(kT)
        Which is equal drawing a RND with 1/2 probablity from 
        N(bias,sigma) or N(-bias,sigma)
    """
    sigma = np.sqrt(kT)
    bias = np.array(bias)
    v = []
    X = random.random()
    if X < 0.5:
        bias = -bias
    for mu in bias:
        v.append(random.gauss(mu, sigma))
    return np.array(v)

def VelNormal(dof, kT):
    """
        Build up a velocity vector by drawing RND from Normal
        distribution with:
            mean(mu)=0.0
            std(sigma)=sqrt(kT)
    """
    sigma = np.sqrt(kT)
    v = []
    for i in range(dof):
        v.append(random.gauss(0.0, sigma))
    return np.array(v)


DrawVel = {
    'Normal': VelNormal,
    'Bimodal_MV': VelBimodal_MV,
    'Bimodal_SV': VelBimodal_SV
}



