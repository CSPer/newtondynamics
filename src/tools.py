
def str2list(str_):
    str_ = str_.lstrip('[').rstrip(']')
    tmp = str_.split(',')
    out = []
    for i in tmp:
        out.append(float(i))
    return out
