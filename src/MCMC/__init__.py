from MCMC.MEHMC import MEHMC_MOVavg, MEHMC_MDavg
from MCMC.HMC import HMC
from MCMC.DFHMC import DFHMC

MCMC = {
    'hmc' : HMC,
    'mehmc_mdavg' : MEHMC_MDavg,
    'mehmc_movavg' : MEHMC_MOVavg,
    'dfhmc': DFHMC
}