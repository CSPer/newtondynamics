"""
    Copyright(c) Can Pervane
    Author  : Can Pervane
    Date    : 27 Feb 2016
    Name    : HMC.py
    Purpose : Hybrid Monte Carlo method which
              uses the Metropolis-Hasting Algorithm
"""

import numpy as np
from integrators.VV import MD_VV
from VelDistribution import DrawVel
from MCMC.AcceptCriteria import AcceptTest
import fileIO.write as log
from tools import str2list
import time

def BiasHMC(param):
    # Initializing the parameters
    kT=float(param['kT'])
    MDSteps=int(param['MDSteps'])
    dt=float(param['dt'])
    HMCSteps=int(param['HMCSteps'])
    x0 = str2list(param['x0'])
    bias = str2list(param['bias'])
    trjout = int(param['trjout'])
    trjfile = param['trjfile']
    logfile = param['logfile']

    # loggin the parameters to a file
    log.writeLog_mess('BiasHMC Simulation Log', filename=logfile)
    log.writeLog_var('x0',x0, filename=logfile)
    log.writeLog_var('Bias',bias, filename=logfile)
    log.writeLog_var('HMCSteps',HMCSteps, filename=logfile)
    log.writeLog_var('MDSteps',MDSteps, filename=logfile)
    log.writeLog_var('dt',dt, filename=logfile)
    log.writeLog_var('kT',kT, filename=logfile)
    log.writeLog_var('trjout',trjout, filename=logfile)

    # HMC algorithm
    sum_ = 0
    dof = len(x0)
    x=np.array(x0)
    start = time.time()
    for i in range(HMCSteps):
        # Propagate the System randomly
        v = DrawVel['Bimodal_MV'](bias,kT)
        x_ ,v_ = MD_VV(MDSteps, x, v, dt)
        # Decide whether or not to accept the proposed state
        accept = AcceptTest['Bimodal_MV'](x, x_, v, v_, bias, kT)
        sum_ = int(accept) + sum_
        if accept:
            x = x_
            v = v_
        # Write the state to a file
        if (i+1) % trjout == 0:
            log.writeTrj(x, v, (i+1), filename=trjfile)
    # loggin hmc output data
    end = time.time()
    acceptance_prob = 100*(sum_/float(HMCSteps))
    log.writeLog_var('Number of HMC steps done',HMCSteps, filename=logfile)
    log.writeLog_var('Acceptance Probability',acceptance_prob, filename=logfile)
    log.writeLog_var('Total Time (s)',end-start, filename=logfile)
    log.writeLog_var('Time (s) / HMCStep',(end-start)/float(HMCSteps), filename=logfile)
    return x
