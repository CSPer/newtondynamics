
import numpy as np
import random
from potential import Ep, Ek


def AcceptTest_Bimodal_SV(x, x_, v, v_, bias, kT, pot='twoWell'):
    beta = 1.0/ kT
    Epot = Ep[pot](x)
    Epot_ = Ep[pot](x_)
    Ekin = Ek(v)
    Ekin_ = Ek(v_)
    Px = np.exp(-beta*Epot)
    Px_ = np.exp(-beta*Epot_)
    Pv = np.exp(-beta*Ekin)
    Pv_ = np.exp(-beta*Ekin_)

    for i in range(len(bias)):
        aug_ = (np.exp(beta*bias[i]*v_[i]) + np.exp(-beta*bias[i]*v_[i]))
        aug = (np.exp(beta*bias[i]*v[i]) + np.exp(-beta*bias[i]*v[i]))
        Pv = Pv * aug
        Pv_ = Pv_ *aug_

    if Px*Pv < 10**(-100):
        accept = 1.0
    else:
        accept = (Px_ * Pv_) / (Px * Pv)
    if accept >= 1.0 or random.random() < accept:
        return True
    else:
        return False

def AcceptTest_Bimodal_MV(x, x_, v, v_, bias, kT, pot='twoWell'):
    beta = 1.0/ kT
    Epot = Ep[pot](x)
    Epot_ = Ep[pot](x_)
    Ekin = Ek(v)
    Ekin_ = Ek(v_)
    Etot = Ekin + Epot
    Etot_ = Ekin_ + Epot_

    deltaE = -beta*(Etot_ - Etot)
    aug = 0
    aug_ = 0
    for i in range(len(bias)):
        aug = aug + bias[i]*v[i]
        aug_ = aug_ + bias[i]*v_[i]

    aug = beta*aug
    aug_ = beta*aug_
    if abs(aug) > 20 or abs(aug_) > 20:
        """
            |x| > 20 => exp(-|x|) ~ 0
        """
        deltaB = abs(aug_)-abs(aug)
        accept = np.exp(deltaE + deltaB)
    else:   
        Pv = np.exp(-aug) + np.exp(aug)
        Pv_ = np.exp(-aug_) + np.exp(aug_)
        accept_bias = Pv_ / Pv
        accept = np.exp(deltaE) * accept_bias

    if accept >= 1.0 or random.random() < accept:
        return True
    else:
        return False

def AcceptTest_Normal(x, x_, v, v_, kT, pot='twoWell'):
    beta = 1.0/kT
    Epot = Ep[pot](x)
    Epot_ = Ep[pot](x_)
    Ekin = Ek(v)
    Ekin_ = Ek(v_)
    Etot = Ekin + Epot
    Etot_ = Ekin_ + Epot_
    accept = np.exp(-beta*(Etot_ - Etot))
    if accept >= 1.0 or random.random() < accept:
        return True
    else:
        return False

AcceptTest = {
    'Normal': AcceptTest_Normal,
    'Bimodal_MV': AcceptTest_Bimodal_MV,
    'Bimodal_SV': AcceptTest_Bimodal_SV
}