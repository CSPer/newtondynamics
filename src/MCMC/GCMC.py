"""
    Copyright(c) Can Pervane
    Author  : Can Pervane
    Date    : 27 Feb 2016
    Name    : HMC.py
    Purpose : Hybrid Monte Carlo method which
              uses the Metropolis-Hasting Algorithm
"""

import numpy as np
import random
from integrators import MD
from VelDistribution import DrawVel
from MCMC.AcceptCriteria import AcceptTest
import fileIO.write as log
from tools import str2list
import time
from progressbar import ProgressBar
import matplotlib.pyplot as plt


def GCMC(param, seed):
    pbar = ProgressBar()
    random.seed(seed)
    
    kT=float(param['kT'])
    MDSteps=int(param['MDSteps'])
    GCMCSteps=int(param['GCMCSteps'])
    dt=float(param['dt'])
    x0 = str2list(param['x0'])
    trjout = int(param['trjout'])
    trjfile = param['trjfile']
    logfile = param['logfile']
    integrator = param['integrator']

    trjfile = trjfile.rsplit('.',1)
    trjfile = ''.join([trjfile[0], '_', str(seed), '.', trjfile[1]])

    logfile = logfile.rsplit('.',1)
    logfile =''.join([logfile[0], '_', str(seed), '.', logfile[1]])

    # loggin the parameters to a file
    log.writeLog_mess('GCMC Simulation Log', filename=logfile)
    log.writeLog_var('x0',x0, filename=logfile)
    log.writeLog_var('integrator',integrator, filename=logfile)
    log.writeLog_var('MDSteps',MDSteps, filename=logfile)
    log.writeLog_var('GCMCSteps',GCMCSteps, filename=logfile)
    log.writeLog_var('dt',dt, filename=logfile)
    log.writeLog_var('kT',kT, filename=logfile)
    log.writeLog_var('trjout',trjout, filename=logfile)
    log.writeLog_var('seed',seed, filename=logfile)

    # GCMC Algorithm
    sum_ = 0
    dof = len(x0)
    x = np.array(x0)
    trjx = []
    start = time.time()
    for i in pbar(range(GCMCSteps)):
        #HMC (translate)
        v = DrawVel['Normal'](dof,kT)
        x_, v_= MD[integrator](MDSteps, x, v, dt, potential='harmonic')
        accept = AcceptTest['Normal'](x, x_, v, v_, kT, pot='harmonic')
        sum_ = int(accept) + sum_
        if accept:
            x = x_
        if ((i+1) % trjout == 0):
            log.writeTrj(x, v, (i+1), filename=trjfile)
        # Insert

        # delete
    end = time.time()
    acceptance_prob = 100*(sum_/float(GCMCSteps))
    log.writeLog_var('Number of GCMC steps done',GCMCSteps, filename=logfile)
    log.writeLog_var('Acceptance Probability',acceptance_prob, filename=logfile)
    log.writeLog_var('Total Time (s)',end-start, filename=logfile)
    log.writeLog_var('Time (s) / GCMCStep',(end-start)/float(GCMCSteps), filename=logfile)

    # f =  plt.figure(figsize=(10,6), dpi=100)
   
    # ax=f.add_axes([0.1, 0.5, 0.70, 0.30])
    # plt.hist(trjx, 100, normed=True)
    # plt.xlabel('x')
    # plt.ylabel('Frequecny')
    # ax=f.add_axes([0.1, 0.1, 0.70, 0.30])
    # plt.plot(trjx, range(GCMCSteps))
    # plt.gca().invert_yaxis()
    # plt.xlabel('x')
    # plt.ylabel('Step')

    # plt.show()
