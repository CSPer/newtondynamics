"""
    Copyright(c) Can Pervane
    Author  : Can Pervane
    Date    : 27 Feb 2016
    Name    : HMC.py
    Purpose : Hybrid Monte Carlo method which
              uses the Metropolis-Hasting Algorithm
"""

import numpy as np
import random
from integrators import MD_avg, MD
from VelDistribution import DrawVel
from MCMC.AcceptCriteria import AcceptTest
import fileIO.write as log
from tools import str2list
import time
from progressbar import ProgressBar

def fig5(param, seed):
    """
        Calculates the bias by running an additional MD simulation
    """
    pbar = ProgressBar()
    random.seed(seed)
    # Initializing the parameters
    kT=float(param['kT'])
    MDSteps=int(param['MDSteps'])
    dt=float(param['dt'])
    scl=float(param['scl'])
    HMCSteps=int(param['HMCSteps'])
    x0 = str2list(param['x0'])
    biasfreq = int(param['biasfreq'])
    trjout = int(param['trjout'])
    trjfile = param['trjfile']
    biasfile = param['biasfile']
    logfile = param['logfile']
    integrator = param['integrator']

    trjfile = trjfile.rsplit('.',1)
    trjfile = ''.join([trjfile[0], '_', str(seed), '.', trjfile[1]])

    logfile = logfile.rsplit('.',1)
    logfile =''.join([logfile[0], '_', str(seed), '.', logfile[1]])

    # loggin the parameters to a file
    describe = (" The average velocity is calculated by running an additional "
                "MD simulation.")
    log.writeLog_mess('MEHMC Simulation Log', filename=logfile)
    log.writeLog_describe(describe, filename=logfile)
    log.writeLog_var('x0',x0, filename=logfile)
    log.writeLog_var('integrator',integrator, filename=logfile)
    log.writeLog_var('HMCSteps',HMCSteps, filename=logfile)
    log.writeLog_var('MDSteps',MDSteps, filename=logfile)
    log.writeLog_var('dt',dt, filename=logfile)
    log.writeLog_var('Bias scale',scl, filename=logfile)
    log.writeLog_var('kT',kT, filename=logfile)
    log.writeLog_var('Bias update Freq (HMCStep)',biasfreq, filename=logfile)
    log.writeLog_var('trjout',trjout, filename=logfile)
    log.writeLog_var('seed',seed, filename=logfile)

    # HMC algorithm
    sum_ = 0
    b = np.array([0.0,0.0])
    dof = len(x0)
    x=np.array(x0)
    start = time.time()
    for i in pbar(range(HMCSteps)):
        # Update bias : Bias is calculated by running extra MD
        if i % biasfreq == 0:
            bias = 0.0*b
        # Propagate the System randomly
        v = DrawVel['Bimodal_MV'](bias,kT)
        b, x_ ,v_ = MD[integrator](MDSteps, x, v, dt, avg_trj=True)
        # Decide whether or not to accept the proposed state
        accept = AcceptTest['Bimodal_MV'](x, x_, v, v_, bias, kT)
        sum_ = int(accept) + sum_
        if accept:
            x = x_
            v = v_
        # Write the state to a file
        if ((i+1) % trjout == 0):
            log.writeTrj(x, v, (i+1), filename=trjfile)
            log.writeBias(b, (i+1), biasfile)
    # loggin hmc output data
    end = time.time()
    acceptance_prob = 100*(sum_/float(HMCSteps))
    log.writeLog_var('Number of HMC steps done',HMCSteps, filename=logfile)
    log.writeLog_var('Acceptance Probability',acceptance_prob, filename=logfile)
    log.writeLog_var('Total Time (s)',end-start, filename=logfile)
    log.writeLog_var('Time (s) / HMCStep',(end-start)/float(HMCSteps), filename=logfile)
    return x

def fig5_(param, seed):
    """
        Calculates the bias by running an additional MD simulation
    """
    pbar = ProgressBar()
    random.seed(seed)
    # Initializing the parameters
    kT=float(param['kT'])
    MDSteps=int(param['MDSteps'])
    dt=float(param['dt'])
    scl=float(param['scl'])
    HMCSteps=int(param['HMCSteps'])
    x0 = str2list(param['x0'])
    biasfreq = int(param['biasfreq'])
    trjout = int(param['trjout'])
    trjfile = param['trjfile']
    biasfile = param['biasfile']
    logfile = param['logfile']
    integrator = param['integrator']

    trjfile = trjfile.rsplit('.',1)
    trjfile = ''.join([trjfile[0], '_', str(seed), '.', trjfile[1]])

    logfile = logfile.rsplit('.',1)
    logfile =''.join([logfile[0], '_', str(seed), '.', logfile[1]])

    # loggin the parameters to a file
    describe = (" The average velocity is calculated by running an additional "
                "MD simulation.")
    log.writeLog_mess('MEHMC Simulation Log', filename=logfile)
    log.writeLog_describe(describe, filename=logfile)
    log.writeLog_var('x0',x0, filename=logfile)
    log.writeLog_var('integrator',integrator, filename=logfile)
    log.writeLog_var('HMCSteps',HMCSteps, filename=logfile)
    log.writeLog_var('MDSteps',MDSteps, filename=logfile)
    log.writeLog_var('dt',dt, filename=logfile)
    log.writeLog_var('Bias scale',scl, filename=logfile)
    log.writeLog_var('kT',kT, filename=logfile)
    log.writeLog_var('Bias update Freq (HMCStep)',biasfreq, filename=logfile)
    log.writeLog_var('trjout',trjout, filename=logfile)
    log.writeLog_var('seed',seed, filename=logfile)

    # HMC algorithm
    sum_ = 0
    b = np.array([0.0,0.0])
    dof = len(x0)
    x=np.array(x0)
    start = time.time()
    v = np.array([1.0, -1.0])
    for i in pbar(range(HMCSteps)):
        # Propagate the System randomly
        x_ ,v_, b = MD_avg[integrator](MDSteps, x, v, dt,20, b)
        x = x_
        v = v_ 
        if ((i+1) % trjout == 0):
            log.writeTrj(x, v, (i+1), filename=trjfile)
            log.writeBias(b, (i+1), biasfile)
    # loggin hmc output data
    end = time.time()
    acceptance_prob = 100*(sum_/float(HMCSteps))
    log.writeLog_var('Number of HMC steps done',HMCSteps, filename=logfile)
    log.writeLog_var('Acceptance Probability',acceptance_prob, filename=logfile)
    log.writeLog_var('Total Time (s)',end-start, filename=logfile)
    log.writeLog_var('Time (s) / HMCStep',(end-start)/float(HMCSteps), filename=logfile)
    return x