"""
    Copyright(c) Can Pervane
    Author  : Can Pervane
    Date    : 27 Feb 2016
    Name    : HMC.py
    Purpose : Hybrid Monte Carlo method which
              uses the Metropolis-Hasting Algorithm
"""

import numpy as np
import random
from integrators import MD_avg, MD
from VelDistribution import DrawVel
from MCMC.AcceptCriteria import AcceptTest
import fileIO.write as log
from tools import str2list
import time
from progressbar import ProgressBar

def MEHMC_MOVavg(param, seed):
    """
        Calculates the bias using a moving average during the MD
        propagation
    """
    pbar = ProgressBar()
    random.seed(seed)
    # Initializing the parameters
    kT=float(param['kT'])
    MDSteps=int(param['MDSteps'])
    dt=float(param['dt'])
    scl=float(param['scl'])
    HMCSteps=int(param['HMCSteps'])
    x0 = str2list(param['x0'])
    biasfreq = int(param['biasfreq'])
    biasbuffer = int(param['biasbuffer'])
    trjout = int(param['trjout'])
    trjfile = param['trjfile']
    logfile = param['logfile']
    initavg = param['initavg']
    integrator = param['integrator']
    biasavg = param['biasavg']

    trjfile = trjfile.rsplit('.',1)
    trjfile = ''.join([trjfile[0], '_', str(seed), '.', trjfile[1]])

    logfile = logfile.rsplit('.',1)
    logfile =''.join([logfile[0], '_', str(seed), '.', logfile[1]])

    # loggin the parameters to a file
    describe = (" The average velocity is calculated through a moving "
                "average calculated during the MD propagation. " 
                "The full step velocities to be averaged are obtained "
                "by taking the average of the half step velocities.")
    log.writeLog_mess('MEHMC Simulation Log', filename=logfile)
    log.writeLog_describe(describe, filename=logfile)
    log.writeLog_var('x0',x0, filename=logfile)
    log.writeLog_var('integrator',integrator, filename=logfile)
    log.writeLog_var('biasavg',biasavg, filename=logfile)
    log.writeLog_var('initavg',initavg, filename=logfile)
    log.writeLog_var('HMCSteps',HMCSteps, filename=logfile)
    log.writeLog_var('MDSteps',MDSteps, filename=logfile)
    log.writeLog_var('dt',dt, filename=logfile)
    log.writeLog_var('Bias scale',scl, filename=logfile)
    log.writeLog_var('kT',kT, filename=logfile)
    log.writeLog_var('Bias Buffer(Averaging Period)',biasbuffer, filename=logfile)
    log.writeLog_var('Bias update Freq (HMCStep)',biasfreq, filename=logfile)
    log.writeLog_var('trjout',trjout, filename=logfile)
    log.writeLog_var('seed',seed, filename=logfile)

    # HMC algorithm
    sum_ = 0
    b = np.array([0.0,0.0])
    dof = len(x0)
    x=np.array(x0)
    start = time.time()
    for i in pbar(range(HMCSteps)):
        # Update bias : Bias is calculated as a moving average
        if i % biasfreq == 0:
            bias = scl*b
        if initavg == 'yes':
            b = np.array([0.0,0.0])
        # Propagate the System randomly
        v = DrawVel['Bimodal_MV'](bias,kT)
        x_ ,v_, b_ = MD_avg[integrator](MDSteps, x, v, dt, biasbuffer, b)
        # Decide whether or not to accept the proposed state
        accept = AcceptTest['Bimodal_MV'](x, x_, v, v_, bias, kT)
        sum_ = int(accept) + sum_
        if accept:
            x = x_
            v = v_
            b = b_
        # Write the state to a file
        if (i+1) % trjout == 0:
            log.writeTrj(x, v, (i+1), filename=trjfile)
    # loggin hmc output data
    end = time.time()
    acceptance_prob = 100*(sum_/float(HMCSteps))
    log.writeLog_var('Number of HMC steps done',HMCSteps, filename=logfile)
    log.writeLog_var('Acceptance Probability',acceptance_prob, filename=logfile)
    log.writeLog_var('Total Time (s)',end-start, filename=logfile)
    log.writeLog_var('Time (s) / HMCStep',(end-start)/float(HMCSteps), filename=logfile)
    return x


def MEHMC_MDavg(param, seed):
    """
        Calculates the bias by running an additional MD simulation
    """
    pbar = ProgressBar()
    random.seed(seed)
    # Initializing the parameters
    kT=float(param['kT'])
    MDSteps=int(param['MDSteps'])
    dt=float(param['dt'])
    scl=float(param['scl'])
    HMCSteps=int(param['HMCSteps'])
    x0 = str2list(param['x0'])
    biasfreq = int(param['biasfreq'])
    biasbuffer = int(param['biasbuffer'])
    trjout = int(param['trjout'])
    trjfile = param['trjfile']
    logfile = param['logfile']
    initavg = param['initavg']
    integrator = param['integrator']
    biasavg = param['biasavg']

    trjfile = trjfile.rsplit('.',1)
    trjfile = ''.join([trjfile[0], '_', str(seed), '.', trjfile[1]])

    logfile = logfile.rsplit('.',1)
    logfile =''.join([logfile[0], '_', str(seed), '.', logfile[1]])

    # loggin the parameters to a file
    describe = (" The average velocity is calculated by running an additional "
                "MD simulation.")
    log.writeLog_mess('MEHMC Simulation Log', filename=logfile)
    log.writeLog_describe(describe, filename=logfile)
    log.writeLog_var('x0',x0, filename=logfile)
    log.writeLog_var('integrator',integrator, filename=logfile)
    log.writeLog_var('biasavg',biasavg, filename=logfile)
    log.writeLog_var('initavg',initavg, filename=logfile)
    log.writeLog_var('HMCSteps',HMCSteps, filename=logfile)
    log.writeLog_var('MDSteps',MDSteps, filename=logfile)
    log.writeLog_var('dt',dt, filename=logfile)
    log.writeLog_var('Bias scale',scl, filename=logfile)
    log.writeLog_var('kT',kT, filename=logfile)
    log.writeLog_var('Bias Buffer(Averaging Period)',biasbuffer, filename=logfile)
    log.writeLog_var('Bias update Freq (HMCStep)',biasfreq, filename=logfile)
    log.writeLog_var('trjout',trjout, filename=logfile)
    log.writeLog_var('seed',seed, filename=logfile)

    # HMC algorithm
    sum_ = 0
    b = np.array([0.0,0.0])
    dof = len(x0)
    x=np.array(x0)
    start = time.time()
    v = DrawVel['Normal'](dof, kT)
    for i in pbar(range(HMCSteps)):
        # Update bias : Bias is calculated as a moving average
        if i % biasfreq == 0:
            b = MD[integrator](biasbuffer, x, v, dt, avg=True)
            bias = scl*b
        # Propagate the System randomly
        v = DrawVel['Bimodal_MV'](bias,kT)
        x_ ,v_ = MD[integrator](MDSteps, x, v, dt)
        # Decide whether or not to accept the proposed state
        accept = AcceptTest['Bimodal_MV'](x, x_, v, v_, bias, kT)
        sum_ = int(accept) + sum_
        if accept:
            x = x_
            v = v_
        # Write the state to a file
        if (i+1) % trjout == 0:
            log.writeTrj(x, v, (i+1), filename=trjfile)
    # loggin hmc output data
    end = time.time()
    acceptance_prob = 100*(sum_/float(HMCSteps))
    log.writeLog_var('Number of HMC steps done',HMCSteps, filename=logfile)
    log.writeLog_var('Acceptance Probability',acceptance_prob, filename=logfile)
    log.writeLog_var('Total Time (s)',end-start, filename=logfile)
    log.writeLog_var('Time (s) / HMCStep',(end-start)/float(HMCSteps), filename=logfile)
    return x

