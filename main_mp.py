"""
    Copyright(c) Can Pervane
    Author  : Can Pervane
    Date    : 25 Feb 2016
    Name    : main.py
    Purpose : it acts as an interface to the code in the src
"""

# import os
# import sys
# MAIN_DIR = os.path.dirname(os.path.abspath(__file__))
# sys.path.append(os.path.join(MAIN_DIR, 'src'))

import random
import argparse
import functools
from multiprocessing import Pool
from MCMC import MCMC
from fileIO.read import parse_mdp
from sys import exit


parser = argparse.ArgumentParser()
req_grp = parser.add_argument_group(title='Required Optional')
req_grp.add_argument("-f", "--file_name", required=True, help="enter name of .mdp file")
req_grp.add_argument("-np", "--n_process", type=int, required=True, help="enter number of parallel processes")

args = parser.parse_args()


param=parse_mdp(args.file_name)
simulation = param['simulation']
if simulation == 'mehmc':
    biasavg = param['biasavg']
    if biasavg == 'moving':
        simulation = 'mehmc_movavg'
    elif biasavg == 'fullmd':
        simulation = 'mehmc_mdavg'
    else:
        print 'biasavg, should be either moving or fullmd'
        exit()

np = args.n_process
pool = Pool(processes=np)
simseed = []
seed = 12345342
for i in range(np):
    simseed.append(seed+(2*i))


func = functools.partial(MCMC[simulation], param)
out = pool.map(func, simseed)
